using System.IO;
using System.Threading.Tasks;
using Calculator.Middleware;
using Microsoft.AspNetCore.Http;
using Xunit;

namespace Calculator.Tests {
    public class StatusMiddlewareTests {

        [Fact]
        public async Task StatusMiddleware_CallNext() {
            bool wasCalled = false;

            Task Next(HttpContext context) {
                wasCalled = true;
                return Task.CompletedTask;
            }

            var statusMiddleware = new StatusMiddleware(Next);

            var httpContext = new DefaultHttpContext();
            httpContext.Request.Path = "/notPing";

            await statusMiddleware.Invoke(httpContext);
            
            Assert.True(wasCalled);
        }

        [Fact]
        public async Task StatusMiddleware_CallPing() {
            bool wasCalled = false;

            Task Next(HttpContext context) {
                wasCalled = true;
                return Task.CompletedTask;
            }

            var statusMiddleware = new StatusMiddleware(Next);

            var memoryStream = new MemoryStream();
            var httpContext = new DefaultHttpContext();
            httpContext.Response.Body = memoryStream;
            httpContext.Request.Path = "/ping";

            await statusMiddleware.Invoke(httpContext);
            
            Assert.False(wasCalled);
            
            Assert.Equal(200, httpContext.Response.StatusCode);
            Assert.Equal("text/plain", httpContext.Response.ContentType);

            var streamCopy = new MemoryStream(memoryStream.ToArray());
            streamCopy.Seek(0, SeekOrigin.Begin);
            
            using (var reader = new StreamReader(streamCopy)) {
                var body = await reader.ReadToEndAsync();
            
                Assert.Equal("pong", body);
            }
            
            memoryStream.Dispose();
        }
        
    }
}