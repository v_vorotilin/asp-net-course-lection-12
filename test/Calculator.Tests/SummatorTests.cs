using System;
using Calculator.Services;
using Xunit;

namespace Calculator.Tests {
    public class SummatorTests {

        [Theory]
        [InlineData(1.5, 2, 3.5)]
        [InlineData(3, 5, 8)]
        [InlineData(double.NegativeInfinity, 1e100, double.NegativeInfinity)]
        [InlineData(double.PositiveInfinity, double.NegativeInfinity, double.NaN)]
        public void Sum_Doubles(double a, double b, double expected) {
            // A1: Arrange

            ISummator summator = new Summator();

            // A2: Act

            var result = summator.Sum(a, b);
            
            // A3: Assert

            Assert.IsType<double>(result);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("1.5", "2", "3.5")]
        [InlineData("3", "5", "8")]
        [InlineData("-100", "100", "0")]
        [InlineData("-1000", "-100", "-1100")]
        public void Sum_Strings(string a, string b, string expected) {
            // A1: Arrange

            ISummator summator = new Summator();
            
            // A2: Act

            var result = summator.Sum(a, b);
            
            // A3: Assert
            
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Sum_StringsNullRef() {
            Assert.Throws<ArgumentNullException>(() => new Summator().Sum("1", null));
        }

        [Fact]
        public void Sum_StringsFormatException() {
            Assert.Throws<FormatException>(() => new Summator().Sum("-asd1", "2"));
        }

        [Fact]
        public void Sum_StringsFormatExceptionUkrainianCulture() {
            Assert.Throws<FormatException>(() => new Summator().Sum("1,5", "2"));
        }
    }
}