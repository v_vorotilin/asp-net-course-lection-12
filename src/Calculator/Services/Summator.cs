using System;
using System.Globalization;

namespace Calculator.Services {
    public class Summator : ISummator {
        public double Sum(double a, double b) {
            return a + b;
        }

        public int Sum(int a, int b) {
            throw new System.NotImplementedException();
        }

        public string Sum(string a, string b) {
            if (a == null || b == null)
                throw new ArgumentNullException();
            
            if (a.Contains(',') ||
                b.Contains(','))
                throw new FormatException();
            
            if (!double.TryParse(a, out var da) || !double.TryParse(b, out var db))
                throw new FormatException();
            
            return (da + db).ToString(CultureInfo.InvariantCulture);
        }
    }
}