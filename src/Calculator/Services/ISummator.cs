namespace Calculator.Services {
    public interface ISummator {
        double Sum(double a, double b);
        int Sum(int a, int b);
        string Sum(string a, string b);
    }
}