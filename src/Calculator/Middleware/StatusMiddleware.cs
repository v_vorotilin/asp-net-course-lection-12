using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Calculator.Middleware {
    public class StatusMiddleware {
        private readonly RequestDelegate _next;
        
        public StatusMiddleware(RequestDelegate next) {
            _next = next;
        }

        public async Task Invoke(HttpContext context) {
            if (context.Request.Path != "/ping")
                await _next(context);

            context.Response.ContentType = "text/plain";
            context.Response.StatusCode = StatusCodes.Status200OK;
            
            using (var stream = new StreamWriter(context.Response.Body)) {
                await stream.WriteAsync("pong");
            }
        }
    }
}